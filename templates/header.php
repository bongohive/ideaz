<nav class="banner navbar navbar-default navbar-fixed-top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="brand" href="<?= esc_url(home_url('/')); ?>"><h1 class="sr-only"><?php bloginfo('name'); ?></h1><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/logo.svg"></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse" role="navigation">
      <?php
      wp_nav_menu( array(
          'menu'              => 'menu',
          'theme_location'    => 'primary_navigation',
          'depth'             => 2,
          'container'         => 'div',
          'container_class'   => 'navbar-collapse collapse',
          'menu_class'        => 'nav navbar-nav navbar-right',
          'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
          'walker'            => new wp_bootstrap_navwalker())
      );
      ?>
    </div>
  </div>
</nav>
