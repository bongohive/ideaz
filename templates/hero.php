  <section id="hero">
    <div class="owl-carousel">
      <?php
        $temp = $wp_query;
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $post_per_page = 3; // -1 shows all posts
        $args = array(
          'post_type' => 'hero',
          'paged' => $paged,
          'posts_per_page' => $post_per_page,
          'order' => 'DESC'
        );

        $wp_query = new WP_Query($args);

        if( have_posts() ) : while ($wp_query->have_posts()) : $wp_query->the_post();
        $slider_image = '';
        $slider_title = '';
        $postId = get_the_ID();
        $has_featured_image = has_post_thumbnail($postId);
        $slider_title = get_post_meta(get_the_ID(), 'hero_title', true);
        $slider_date  = get_post_meta(get_the_ID(), 'hero_date', true);
        $slider_lead  = get_post_meta(get_the_ID(), 'hero_lead', true);
        $slider_link  = get_post_meta(get_the_ID(), 'hero_link', true);
        $slider_link_text  = get_post_meta(get_the_ID(), 'hero_link_text', true);
        $hero = pods('hero', $postId);
        $slider_image = pods_image(
            $hero->field( 'image', TRUE ),
            'jumbotron',
            0,
            array(
                'alt' => trim( strip_tags( $hero->field( 'name' ) ) ),
                'title' => trim( strip_tags( $hero->field( 'name' ) ) ),
                'class' => 'img-responsive'
            ),
            true
        );
       ?>
      <div class="item">
        <?php echo $slider_image; ?>
        <div class="container">
          <div class="hero-text">
            <h2 class="hero-head"><?php echo $slider_title; ?></h2>
            <h3 class="hero-date"><?php if ($slider_date != '') : echo date('l, j F Y',strtotime($slider_date)); endif; ?></h3>
            <p class="lead"><?php echo $slider_lead; ?></p>
            <?php if($slider_link): ?>
            <a href="<?php echo $slider_link; ?>" title="<?php echo $slider_title; ?>" class="btn btn-primary"><?php echo $slider_link_text; ?></a>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <?php endwhile; else: ?>
      <?php endif; wp_reset_query(); $wp_query = $temp ?>
    </div>
  </section>