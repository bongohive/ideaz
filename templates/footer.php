<footer class="content-info" role="contentinfo">
  <div class="container">
    <div class="row footer-social">
      <div class="col-sm-12 col-md-6">
        <?php dynamic_sidebar('sidebar-footer-left'); ?>
      </div>
      <div class="col-sm-12 col-md-6">
        <?php dynamic_sidebar('sidebar-footer-right'); ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <?php dynamic_sidebar('sidebar-footer-top'); ?>
        <?php dynamic_sidebar('sidebar-footer'); ?>
      </div>
    </div>
  </div>
</footer>
